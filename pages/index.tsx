import { Button, Container, FormControl, FormLabel, Heading, Input, Modal, ModalBody, ModalCloseButton, ModalContent, ModalFooter, ModalHeader, ModalOverlay, Stack, Table, TableContainer, Tag, Tbody, Td, Th, Thead, Tr } from '@chakra-ui/react';
import { useContractWrite, usePrepareContractWrite } from 'wagmi'
import { useEffect, useMemo, useState } from 'react';

import { CONTRACT_ADDRESS } from '../src/config/common'
import { Default } from 'components/layouts/Default';
import Moralis from 'moralis';
import type { NextPage } from 'next';
import abi from '../src/config/abi.json'
import dayjs from 'dayjs'
import { useSession } from 'next-auth/react';

const ABI = abi

const HomePage: NextPage = () => {
  const { data } = useSession();

  const [listTask, setListTask] = useState([])
  const [title, setTitle] = useState('')
  const [description, setDescription] = useState('')
  const [startDate, setStartDate] = useState('')
  const [endDate, setEndDate] = useState('')
  const [selectedId, setSelectedId] = useState(0)
  const [isOpenModal, setIsOpenModal] = useState(false)
  const [selectedTask, setSelectedTask] = useState({
    id: '',
    title: '',
    description: '',
    startDate: 0,
    endDate: 0
  })

  const fetchData = async () => {
    const response: any = await Moralis.EvmApi.utils.runContractFunction({
      address: CONTRACT_ADDRESS,
      functionName: 'getAllTask',
      abi: ABI,
      chain: data?.user?.chainId,
      params: {
        userAddress: data?.user?.address.toLowerCase()
      }
    })
    setListTask(response.raw)
  }

  useEffect(() => {
    if (!Moralis?.Core?.isStarted) {
      Moralis.start({
        apiKey: process.env.NEXT_PUBLIC_MORALIS_API_KEY
      })
    }
  }, [])


  const _startDate = useMemo(() => {
    if (startDate) {
      return new Date(startDate).getTime();
    }
    return 0
  }, [startDate])
  const _endDate = useMemo(() => {
    if (endDate) {
      return new Date(endDate).getTime();
    }
    return 0
  }, [endDate])

  const { config: configCreateTask } = usePrepareContractWrite({
    address: CONTRACT_ADDRESS,
    abi: ABI,
    chainId: data?.user?.chainId,
    functionName: 'createTask',
    args: [
      title,
      description,
      _startDate,
      _endDate
    ]
  })
  const { config: configUpdateTask } = usePrepareContractWrite({
    address: CONTRACT_ADDRESS,
    abi: ABI,
    chainId: data?.user?.chainId,
    functionName: 'updateTask',
    args: [
      selectedTask.id,
      selectedTask.title,
      selectedTask.description,
      selectedTask.startDate,
      selectedTask.endDate
    ]
  })
  const { config: configCompleteTask } = usePrepareContractWrite({
    address: CONTRACT_ADDRESS,
    abi: ABI,
    chainId: data?.user?.chainId,
    functionName: 'completeTask',
    args: [Number(selectedId), true]
  })
  const { config: configDeleteTask } = usePrepareContractWrite({
    address: CONTRACT_ADDRESS,
    abi: ABI,
    chainId: data?.user?.chainId,
    functionName: 'deleteTask',
    args: [Number(selectedId)]
  })
  const { write: createTask, isLoading: isLoadingCreate } = useContractWrite(configCreateTask)
  const { write: updateTask, isLoading: isLoadingUpdate } = useContractWrite(configUpdateTask)
  const { write: completeTask } = useContractWrite(configCompleteTask)
  const { write: deletetask, isLoading: isLoadingDelete } = useContractWrite(configDeleteTask)

  const handleCreateTask = async () => {
    await createTask?.()
  }
  const handleCompleteTask = async (id: any) => {
    setSelectedId(id)
    setTimeout(() => {
      completeTask?.()
    }, 3000);
  }
  const handleDeleteTask = async (id: any) => {
    setSelectedId(id)
    setTimeout(() => {
      deletetask?.()
    }, 3000);
  }

  useEffect(() => {
    if (data?.user) {
      console.log(data);

      fetchData()
    }
  }, [data])

  const openModalUpdate = (task: any) => {
    const value: any = {
      id: task[0],
      title: task[1],
      description: task[2],
      startDate: task[3],
      endDate: task[4]
    }
    setSelectedTask(value)
    setIsOpenModal(true)

  }

  const onChangeForm = (key: string, value: any) => {
    setSelectedTask((prev) => {
      switch (key) {
        case 'title':
          return {
            ...prev,
            title: value
          }
        case 'description':
          return {
            ...prev,
            description: value
          }
        case 'startDate':
          return {
            ...prev,
            startDate: value
          }
        case 'endDate':
          return {
            ...prev,
            endDate: value
          }

        default:
          return prev
      }
    })

  }
  const onCloseModal = () => {
    setIsOpenModal(false)
  }

  return (
    <Default pageName="Home">
      {data?.user
        ?
        <Container pb={30} maxWidth="1200px">
          <FormControl>
            <FormLabel>Title</FormLabel>
            <Input value={title} onChange={(e) => setTitle(e.target.value)} type='text' placeholder='Please input title' mb={5} />
            <FormLabel>Description</FormLabel>
            <Input value={description} onChange={(e) => setDescription(e.target.value)} type='text' placeholder='Please input description' mb={5} />
            <FormLabel>Start date</FormLabel>
            <Input value={startDate} onChange={(e) => setStartDate(e.target.value)} type='date' placeholder='Please input start date' mb={5} />
            <FormLabel>End date</FormLabel>
            <Input value={endDate} onChange={(e) => setEndDate(e.target.value)} type='date' placeholder='Please input end date' mb={5} />
          </FormControl>
          <Button onClick={handleCreateTask} isLoading={isLoadingCreate}>Submit</Button>

          <Heading size={'xl'} mt={20} mb={5}>Danh sách task</Heading>
          <TableContainer>
            <Table variant='simple'>
              <Thead>
                <Tr>
                  <Th>Id</Th>
                  <Th>Title</Th>
                  <Th>Description</Th>
                  <Th>Start date</Th>
                  <Th>End date</Th>
                  <Th>Status</Th>
                  <Th align='center'>Action</Th>
                </Tr>
              </Thead>
              <Tbody>
                {listTask.map((task: any, index) => (
                  <Tr key={index}>
                    <Td>{task[0]}</Td>
                    <Td>{task[1]}</Td>
                    <Td>{task[2]}</Td>
                    <Td>{dayjs(Number(task[3])).format('DD/MM/YYYY')}</Td>
                    <Td>{dayjs(Number(task[4])).format('DD/MM/YYYY')}</Td>
                    <Td>
                      <Tag size="md" variant='solid' colorScheme={task[5] ? 'green' : 'red'}>
                        {task[5] ? 'Completed' : 'Inprogress'}
                      </Tag>
                    </Td>
                    <Td width={200} align='center'>
                      <Stack direction='row' spacing={4} align='center'>
                        <Button colorScheme='red' variant='ghost' onClick={() => openModalUpdate(task)}>
                          View
                        </Button>
                        <Button colorScheme='red' variant='ghost' onClick={() => handleCompleteTask(task[0])}>
                          Make complete
                        </Button>
                        <Button colorScheme='red' disabled={isLoadingDelete} variant='ghost' onClick={() => handleDeleteTask(task[0])}>
                          Delete
                        </Button>
                      </Stack>
                    </Td>
                  </Tr>
                ))}
              </Tbody>
            </Table>
          </TableContainer>
          <Modal isOpen={isOpenModal} onClose={onCloseModal}>
            <ModalOverlay />
            <ModalContent>
              <ModalHeader>Modal Title</ModalHeader>
              <ModalCloseButton />
              <ModalBody>
                <FormControl>
                  <FormLabel>Title</FormLabel>
                  <Input value={selectedTask.title} onChange={(e) => onChangeForm('title', e.target.value)} type='text' placeholder='Please input title' mb={5} />
                  <FormLabel>Description</FormLabel>
                  <Input value={selectedTask.description} onChange={(e) => onChangeForm('description', e.target.value)} type='text' placeholder='Please input description' mb={5} />
                  <FormLabel>Start date</FormLabel>
                  <Input value={selectedTask.startDate} onChange={(e) => onChangeForm('startDate', e.target.value)} type='date' placeholder='Please input start date' mb={5} />
                  <FormLabel>End date</FormLabel>
                  <Input value={selectedTask.endDate} onChange={(e) => onChangeForm('endDate', e.target.value)} type='date' placeholder='Please input end date' mb={5} />
                </FormControl>
              </ModalBody>

              <ModalFooter>
                <Button colorScheme='blue' mr={3} onClick={onCloseModal}>
                  Close
                </Button>
                <Button variant='ghost' isLoading={isLoadingUpdate} onClick={() => updateTask?.()}>Update</Button>
              </ModalFooter>
            </ModalContent>
          </Modal>
        </Container>
        :
        <Heading size={'2xl'}>Please connect to your wallet</Heading>
      }
    </Default >
  );
};

export default HomePage;
